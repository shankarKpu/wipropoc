package com.wipro.androidcasestudy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("rows")
    @Expose
    private List<RowModel> rows = null;
    public final static Parcelable.Creator<ResponseModel> CREATOR = new Creator<ResponseModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ResponseModel createFromParcel(Parcel in) {
            return new ResponseModel(in);
        }

        public ResponseModel[] newArray(int size) {
            return (new ResponseModel[size]);
        }

    }
            ;

    protected ResponseModel(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.rows, (RowModel.class.getClassLoader()));
    }

    public ResponseModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<RowModel> getRows() {
        return rows;
    }

    public void setRows(List<RowModel> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeList(rows);
    }

    public int describeContents() {
        return 0;
    }


}