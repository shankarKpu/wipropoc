package com.wipro.androidcasestudy.data.remote;


import com.wipro.androidcasestudy.data.model.ResponseModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

import static com.wipro.androidcasestudy.data.remote.ApiEndPoint.FEEDS;

public interface ApiHelper {

    @GET ( FEEDS )
    Observable<ResponseModel> getFeeds();




}
