package com.wipro.androidcasestudy.data.remote;


import com.wipro.androidcasestudy.data.model.ResponseModel;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppApiHelper implements ApiHelper {

    private final ClientModuleImplement mClientModule;
    private SchedulerProvider mSchedulerProvider;

    @Inject
    public AppApiHelper(ClientModuleImplement clientModule,
                        SchedulerProvider schedulerProvider
    ) {
        this.mClientModule = clientModule;
        this.mSchedulerProvider = schedulerProvider;

    }

    @Override
    public Observable<ResponseModel> getFeeds() {
        return mClientModule.createRetrofitBody( ).getFeeds( );
    }

}
