package com.wipro.androidcasestudy.data;

import android.content.Context;

import com.google.gson.Gson;
import com.wipro.androidcasestudy.data.model.ResponseModel;
import com.wipro.androidcasestudy.data.remote.ApiHelper;
import com.wipro.androidcasestudy.data.remote.ClientModuleImplement;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppDataManager implements DataManager {

    private final Context mContext;

    private final Gson mGson;

    private final ClientModuleImplement mClientModuleImplement;

    @Inject
    public AppDataManager(@ApplicationContext Context context, ClientModuleImplement
            clientModule, Gson gson) {
        mContext = context;
        mGson = gson;
        mClientModuleImplement = clientModule;
    }



      public ApiHelper createRetrofitBody() {
        return mClientModuleImplement.createRetrofitBody( );
    }



    @Override
    public Observable<ResponseModel> getFeeds() {
        return createRetrofitBody( ).getFeeds( );
    }

   
}
