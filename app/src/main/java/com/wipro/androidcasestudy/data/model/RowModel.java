package com.wipro.androidcasestudy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.wipro.androidcasestudy.utils.AppConstants;

public class RowModel implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imageHref")
    @Expose
    private String imageHref;
    public final static Parcelable.Creator<RowModel> CREATOR = new Creator<RowModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RowModel createFromParcel(Parcel in) {
            return new RowModel(in);
        }

        public RowModel[] newArray(int size) {
            return (new RowModel[size]);
        }

    }
            ;

    protected RowModel(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.imageHref = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RowModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        if(title.isEmpty() || title==null){
            this.title =AppConstants.NO_TITLE;
        }

        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {

        if(description.isEmpty() || description ==null){
            this.description=AppConstants.NO_DESCRIPTION;
        }
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(imageHref);
    }

    public int describeContents() {
        return 0;
    }


}
