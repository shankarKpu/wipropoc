package com.wipro.androidcasestudy.data.remote;

public interface ClientModuleImplement {

    ApiHelper createRetrofitBody();
}
