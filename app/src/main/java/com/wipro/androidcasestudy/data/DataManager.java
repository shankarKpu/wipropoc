package com.wipro.androidcasestudy.data;
import com.wipro.androidcasestudy.data.model.ResponseModel;
import com.wipro.androidcasestudy.data.remote.ApiHelper;

import io.reactivex.Observable;

public interface DataManager extends ApiHelper {


    Observable<ResponseModel> getFeeds();
}
