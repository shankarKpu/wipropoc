package com.wipro.androidcasestudy.ui.landing;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.wipro.androidcasestudy.commom.ViewModelProviderFactory;
import com.wipro.androidcasestudy.data.DataManager;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class FeedListActivityModule {
    @Provides
    LinearLayoutManager provideLinearLayoutManager(@ApplicationContext Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    FeedsItemAdapter provideOpenSourceAdapter(@ApplicationContext Context context) {
        return new FeedsItemAdapter(context);
    }
    @Provides
    ViewModelProvider.Factory mainViewModelProvider(FeedListViewModel appLandingModelView) {
        return new ViewModelProviderFactory<>(appLandingModelView);
    }

    @Provides
    static FeedListViewModel providesLandingModelView(@ApplicationContext Context context,DataManager dataManager, SchedulerProvider schedulerProvider){

        return new FeedListViewModel(context, dataManager,schedulerProvider ) ;
    }
}
