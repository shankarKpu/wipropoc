package com.wipro.androidcasestudy.ui.landing;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.wipro.androidcasestudy.data.model.RowModel;
import com.wipro.androidcasestudy.databinding.EmptyItemViewModelBinding;
import com.wipro.androidcasestudy.databinding.FeedItemsBinding;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;
import com.wipro.androidcasestudy.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FeedsItemAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<RowModel> mResponseList;
    private final Context mContext;


    private FeedAdapterListener mListener;

    public FeedsItemAdapter(@ApplicationContext Context context) {
        this.mContext = context;
        this.mResponseList = new ArrayList<>();
    }


    @Override
    public int getItemCount() {
        if (!mResponseList.isEmpty()) {
            return mResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:

                FeedItemsBinding feedItemsBinding = FeedItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                parent.setTag(feedItemsBinding);
                return new FeedListViewHolder(feedItemsBinding);


            case VIEW_TYPE_EMPTY:
            default:
                EmptyItemViewModelBinding emptyViewBinding = EmptyItemViewModelBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<RowModel> repoList) {
        mResponseList.addAll(repoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mResponseList.clear();
        notifyDataSetChanged();
    }

    public void setListener(FeedAdapterListener listener) {
        this.mListener = listener;
    }

    public interface FeedAdapterListener {

        void onRetryClick();
    }

    public class EmptyViewHolder extends BaseViewHolder implements EmptyItemViewModel.EmptyItemViewModelListener {

        private final EmptyItemViewModelBinding mBinding;

        public EmptyViewHolder(EmptyItemViewModelBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            EmptyItemViewModel emptyItemViewModel = new EmptyItemViewModel(this);
            mBinding.setEmptyFeedView(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }

    public class FeedListViewHolder extends BaseViewHolder implements ItemViewModel.FeedItemListener {


        private RowModel rowModel;
        private FeedItemsBinding itemViewModelBinding;
        private ItemViewModel itemViewModel;


        public FeedListViewHolder(FeedItemsBinding binding) {
            super(binding.getRoot());
            this.itemViewModelBinding = binding;
        }

        @Override
        public void onBind(int position) {
            rowModel = mResponseList.get(position);
            itemViewModel = new ItemViewModel(rowModel, this);
            itemViewModelBinding.setFeedItemViewModel(itemViewModel);
            itemViewModelBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(RowModel datum) {
        }


    }
}
