package com.wipro.androidcasestudy.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.wipro.androidcasestudy.BR;
import com.wipro.androidcasestudy.R;
import com.wipro.androidcasestudy.databinding.SplashActivityBinding;
import com.wipro.androidcasestudy.ui.base.BaseActivity;
import com.wipro.androidcasestudy.ui.landing.FeedListActivity;
import com.wipro.androidcasestudy.utils.NetworkUtil;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<SplashActivityBinding, SplashViewModel>
        implements SplashNavigator {

    private static final long SPLASH_TIME_OUT = 5000;
    @Inject
    SplashViewModel mSplashViewModel;

    SplashActivityBinding mSplashScreenBinding;

    @Override
    public int getBindingVariable() {
        return BR.splashview;
    }

    @Override
    public int getLayoutId() {
        return R.layout.splash_screen;
    }

    @Override
    public SplashViewModel getViewModel() {
        return mSplashViewModel;
    }

    @Override
    public void intiBinding() {
        mSplashScreenBinding = getViewDataBinding();
        mSplashViewModel.setNavigator(this);
        mSplashViewModel.openNextActivity();

    }

    @Override
    public void openNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(FeedListActivity.newIntent(SplashActivity.this));
                finish();
            }
        }, SPLASH_TIME_OUT);


    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        intiBinding();

    }


}
