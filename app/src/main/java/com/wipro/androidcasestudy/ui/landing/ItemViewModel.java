package com.wipro.androidcasestudy.ui.landing;

import android.databinding.ObservableField;
import android.support.annotation.Nullable;

import com.wipro.androidcasestudy.data.model.RowModel;
import com.wipro.androidcasestudy.utils.AppConstants;

public class ItemViewModel {
    @Nullable
    public ObservableField<String> mTitle;

    public ObservableField<String> mImageUrl;

    @Nullable
    public ObservableField<String> mDescription;


    public final FeedItemListener mListener;

    private RowModel mData;

    public ItemViewModel(RowModel data, FeedItemListener listener) {
        this.mData = data;
        this.mListener = listener;

        if (mData.getTitle() != null) {
            mTitle = new ObservableField<String>(mData.getTitle());

        } else {
            mTitle = new ObservableField<String>(AppConstants.NO_TITLE);
        }
        if (mData.getDescription() != null) {
            mDescription = new ObservableField<String>(mData.getDescription());
        } else {
            mDescription = new ObservableField<String>(AppConstants.NO_DESCRIPTION);
        }

        mImageUrl = new ObservableField<String>(mData.getImageHref());


    }

    public void onItemClick() {
        mListener.onItemClick(mData);
    }

    public interface FeedItemListener {

        void onItemClick(RowModel item);
    }

}
