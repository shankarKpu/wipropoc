package com.wipro.androidcasestudy.ui.landing;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;

import com.wipro.androidcasestudy.BR;
import com.wipro.androidcasestudy.R;
import com.wipro.androidcasestudy.databinding.ActivityFeedListBinding;
import com.wipro.androidcasestudy.ui.base.BaseActivity;

import javax.inject.Inject;

public class FeedListActivity extends BaseActivity<ActivityFeedListBinding, FeedListViewModel> implements
        FeedsNavigator, FeedsItemAdapter.FeedAdapterListener,SwipeRefreshLayout.OnRefreshListener {

    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    FeedsItemAdapter mFeedsItemAdapter;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private FeedListViewModel mFeedListViewModel;
    private ActivityFeedListBinding mActivityFeedBinding;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, FeedListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }


    @Override
    public int getBindingVariable() {
        return BR.feedsViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.feed_list;
    }

    @Override
    public FeedListViewModel getViewModel() {
        mFeedListViewModel = ViewModelProviders.of(this, mViewModelFactory).get(
                FeedListViewModel.class);
        return mFeedListViewModel;
    }

    @Override
    public void intiBinding() {
        subscribeToLiveData();
        setUp();
        setSupportActionBar(mActivityFeedBinding.toolbar);

    }

    private void setUp() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityFeedBinding.feedsRecyclerView.setLayoutManager(mLayoutManager);
        mActivityFeedBinding.feedsRecyclerView.setAdapter(mFeedsItemAdapter);
        mFeedsItemAdapter.setListener(this);
        mActivityFeedBinding.swipeToRefresh.setOnRefreshListener(this);
//        mActivityFeedBinding.setOnRefreshListener(this);
    }

    private void subscribeToLiveData() {
        mFeedListViewModel.getFeedData().observe(this, datumList -> mFeedListViewModel.addFeedsList(datumList));
        mFeedListViewModel.getTitle().observe(this, this::updateToolBar);
        mFeedListViewModel.showMessage().observe(this, this::showMessage);
        mFeedListViewModel.getSwipeLayoutStatus().observe(this,this::updateSwipeLayout);
    }

    private void updateSwipeLayout(Boolean status) {


       mActivityFeedBinding.swipeToRefresh.setRefreshing ( false );
    }

    private void updateToolBar(String title) {
        mActivityFeedBinding.toolbar.setTitle(title);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityFeedBinding = getViewDataBinding();
        mFeedListViewModel.setNavigator(this);
        intiBinding();


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void showMessage(String mesg) {
        Snackbar snackbar = Snackbar
                .make(mActivityFeedBinding.clRootView, mesg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    @Override
    public void onRetryClick() {
        mFeedListViewModel.getFeeds();
    }

    @Override
    public void onRefresh() {

        mFeedListViewModel.getFeeds();
    }
}
