package com.wipro.androidcasestudy.ui.splash;

import android.content.Context;

import com.wipro.androidcasestudy.data.DataManager;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;
import com.wipro.androidcasestudy.ui.base.BaseViewModel;
import com.wipro.androidcasestudy.utils.NetworkUtil;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {




    public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public void openNextActivity() {
        getNavigator().openNextActivity();


    }




}
