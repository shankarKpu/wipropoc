package com.wipro.androidcasestudy.ui.landing;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.wipro.androidcasestudy.R;
import com.wipro.androidcasestudy.data.DataManager;
import com.wipro.androidcasestudy.data.model.RowModel;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;
import com.wipro.androidcasestudy.ui.base.BaseViewModel;
import com.wipro.androidcasestudy.utils.AppConstants;
import com.wipro.androidcasestudy.utils.NetworkUtil;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

import java.util.List;

public class FeedListViewModel extends BaseViewModel<FeedsNavigator> {

    public MutableLiveData<String> mToolBarTitle = new MutableLiveData<>();

    public final ObservableList<RowModel> mFeedList = new ObservableArrayList<>();

    public MutableLiveData<String> mShowMessage = new MutableLiveData<String>();

    private final MutableLiveData<List<RowModel>> mFeedsData;

    private final MutableLiveData<Boolean> mSwipeLayout = new MutableLiveData<>();

    public Context mContext;

    public FeedListViewModel(@ApplicationContext Context context, DataManager dataManager,
                             SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        mFeedsData = new MutableLiveData<>();
        this.mContext =context;
        getFeeds();
    }

    public void addFeedsList(List<RowModel> item) {
        mFeedList.clear();
        mFeedList.addAll(item);
    }

    public MutableLiveData<List<RowModel>> getFeedData() {
        return mFeedsData;
    }

    public MutableLiveData<String> showMessage() {
        return mShowMessage;
    }

    public MutableLiveData<String> getTitle() {
        return mToolBarTitle;
    }

    public MutableLiveData<Boolean> getSwipeLayoutStatus(){

        return mSwipeLayout;
    }

    public void onRefresh()
    {
        getFeeds();
    }


    public void getFeeds() {
        setIsLoading(true);
        mSwipeLayout.setValue(true);
        if (!NetworkUtil.isConnected(mContext)) {
            mShowMessage.setValue(AppConstants.NETWORK_STATUS);
            setIsLoading(false);

        } else {
            getCompositeDisposable().add(getDataManager()
                    .getFeeds()
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(responseModel -> {
                        if (responseModel != null && responseModel.getRows() != null) {
                            mToolBarTitle.setValue(responseModel.getTitle());
                            mFeedsData.setValue(responseModel.getRows());
                            mSwipeLayout.setValue(false);
                        }
                        setIsLoading(false);
                    }, throwable -> {
                        setIsLoading(false);
                        mSwipeLayout.setValue(false);
                        mShowMessage.setValue(AppConstants.NETWORK_ERROR);

                    }));
        }


    }

}
