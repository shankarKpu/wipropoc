package com.wipro.androidcasestudy.ui.splash;

import com.wipro.androidcasestudy.data.DataManager;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class SplashActivityModule {

    @Provides
    static SplashViewModel providesSplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider ){
        return new SplashViewModel(dataManager,schedulerProvider);
    }
}
