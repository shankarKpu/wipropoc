package com.wipro.androidcasestudy.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wipro.androidcasestudy.data.AppDataManager;
import com.wipro.androidcasestudy.data.DataManager;
import com.wipro.androidcasestudy.data.remote.ApiHelper;
import com.wipro.androidcasestudy.data.remote.AppApiHelper;
import com.wipro.androidcasestudy.data.remote.ClientModule;
import com.wipro.androidcasestudy.data.remote.ClientModuleImplement;
import com.wipro.androidcasestudy.di.scope.ApplicationContext;
import com.wipro.androidcasestudy.utils.rx.AppSchedulerProvider;
import com.wipro.androidcasestudy.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    @ApplicationContext
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Provides
    SchedulerProvider providesSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    DataManager providesDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    ClientModuleImplement providesClientModuleImplement() {
        return new ClientModule();
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }


}
