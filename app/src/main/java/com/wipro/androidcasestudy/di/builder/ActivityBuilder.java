package com.wipro.androidcasestudy.di.builder;


import com.wipro.androidcasestudy.ui.landing.FeedListActivity;
import com.wipro.androidcasestudy.ui.landing.FeedListActivityModule;
import com.wipro.androidcasestudy.ui.splash.SplashActivity;
import com.wipro.androidcasestudy.ui.splash.SplashActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

   @ContributesAndroidInjector(modules = SplashActivityModule.class)
   abstract SplashActivity bindSplashActivity();

   @ContributesAndroidInjector(modules = {FeedListActivityModule.class})
   abstract FeedListActivity bindFeedListActivity();
}

